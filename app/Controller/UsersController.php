<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function beforeFilter() {

        parent::beforeFilter();
        $this->Auth->allow('add');
    }

    public function index() {

        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function login() {

        if ($this->request->is('post')) {

            if ($this->Auth->login()) {

                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout() {

        session_unset();
        session_destroy();
        session_start();
        return $this->redirect($this->Auth->logout());
    }

    public function view($id = null) {

        $this->User->id = $id;
        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        $this->set('user', $this->User->findById($id));
    }

    public function add() {

        if ($this->request->is('post')) {

            $usernameMsg="";
            $size=$this->request->data["User"]["image"]["size"];
            $type=$this->request->data["User"]["image"]["type"];

            if(parent::checkImgSizeType($type,$size)){     
            
                if($this->data && is_uploaded_file($this->request->data['User']['image']['tmp_name'])){

                    $this->request->data["User"]["img_name"] = $this->request->data["User"]["image"]["name"];

                    $fileData = fread(
                    fopen($this->data["User"]["image"]["tmp_name"], "r"),
                    $this->request->data["User"]["image"]["size"]
                    );

                    $this->request->data["User"]["image"] = $fileData;
                }

                $isUsernameExit = $this->User->find('count', array('conditions' =>array('User.username LIKE' => $this->data["User"]["username"])));

                if($isUsernameExit <= 0)
                {

                    $this->User->create();

                    if ($this->User->save($this->request->data["User"])) {

                        $this->Flash->success(__('The user has been saved '));
                        return $this->redirect(array('action' => 'login'));
                    }
                }

                $usernameMsg = "Username is already registered.";
            }

            $this->Flash->error(__('The user could not be saved. Please, try again. '.$usernameMsg));
        }
    }

/*  public function edit($id = null) {

        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->User->save($this->request->data)) {

                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('The user could not be saved. Please, try again.'));

        } else {

            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {

        $this->request->allowMethod('post');
        $this->User->id = $id;

        if (!$this->User->exists()) {

            throw new NotFoundException(__('Invalid user'));
        }

        if ($this->User->delete()) {

            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }*/

        //For Image Upload
    function addImage(){

       // if($this->data &&is_uploaded_file($this->data["File"]["image"]["tmp_name"]) ){

            $this->data["User"]["img_name"] = $this->data["File"]["image"]["name"];

            $fileData = fread(
            fopen($this->data["File"]["image"]["tmp_name"], "r"),
            $this->data["File"]["image"]["size"]
            );

            $this->data["User"]["image"] = $fileData;

            if ($this->User->save($this->data["User"])) {

               $this->Flash->success(__('File uploaded successufully'));

            } else {

                $this->Flash->error(__('File does not uploaded successfully'));
            }
       // }
    }

    function editImage($id=null){

        if ($this->data &&is_uploaded_file($this->data["File"]["image"]["tmp_name"])) {

            $this->data["User"]["img_name"] = $this->data["File"]["image"]["name"];

            $fileData = fread(
            fopen($this->data["File"]["image"]["tmp_name"], “r”),
            $this->data["File"]["image"]["size"]
            );

            $this->data["User"]["image"] = $fileData;

            if ($this->User->save($this->data["User"])){
                $this->Flash->success(__('File uploaded successfully'));
            } else {

                $this->Flash->error(__('File does not uploaded successfully'));
            }
        } else {

            $this->data = $this->User->read(null,$id);
        }
    }

    function image($id){

        $this->data = $this->User->read(null,$id);
        echo $this->data["User"]["image"];
        die;
    }


}