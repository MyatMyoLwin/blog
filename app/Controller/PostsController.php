<?php

App::uses('AppController', 'Controller');

class PostsController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash');
    public $components = array('Flash','Paginator');
    var $name = 'Posts';

     function search() {

        $url['action'] = 'index';

        foreach ($this->data as $k=>$v){ 
            foreach ($v as $kk=>$vv){ 
                if ($vv) {
                    $url[$k.'.'.$kk]=$vv; 
                }
            } 
        }

        $this->redirect($url, null, true);
    }

    public function index() {

        if(isset($this->passedArgs['Search.id'])) {

            $id = $this->passedArgs['Search.id'];
            $conditions = array('Post.id LIKE' => $id);
            $this->request->data['Search']['id'] = $this->passedArgs['Search.id'];
        }

        if(isset($this->passedArgs['Search.title'])) {

            $titles = $this->passedArgs['Search.title'];
            if(!empty($conditions)) {

                $conditions += array('Post.title LIKE' => "%$titles%");
            }
            else{

                 $conditions = array('Post.title LIKE' => "%$titles%");
            }
            $this->request->data['Search']['title'] = $this->passedArgs['Search.title'];
        }

        if($this->request->data==null){

           $conditions = array('not' => array('Post.id' => null));
        }

        $this -> paginate = array(
        	'limit' => 3,
            'conditions'=>$conditions,
        	'order' => array('id' => 'desc')
        );

        $this->set('posts', $this->paginate());
    }

    public function view($id) {

        if (!$id) {

            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);

        if (!$post) {

            throw new NotFoundException(__('Invalid post'));
        }

        if (!empty($this->data['Comment'])) {

            $this->request->data['Comment']['class'] = 'Post'; 
            $this->request->data['Comment']['foreign_id'] = $id; 
            $this->request->data['Comment']['user_id'] = $this->Auth->user('id');

            if(empty($this->data['Comment']['name'])){

                $this->request->data['Comment']['name'] = $this->Auth->user('username'); 
            }

            $size=$this->request->data["Comment"]["image"]["size"];
            $type=$this->request->data["Comment"]["image"]["type"];

            if($this->checkImgSizeType($type,$size)){

                if($this->data && is_uploaded_file($this->request->data['Comment']['image']['tmp_name'])){

                    $this->request->data["Comment"]["img_name"] = $this->request->data["Comment"]["image"]["name"];

                    $fileData = fread(
                    fopen($this->data["Comment"]["image"]["tmp_name"], "r"),
                    $this->request->data["Comment"]["image"]["size"]
                    );

                    $this->request->data["Comment"]["image"] = $fileData;
                    $msgImg = "Image uploaded successfully.";
                }

            }
            else{

                $msgImg = "Image uploaded unsuccessfully.";
                $this->request->data["Comment"]["image"] = NULL;
            }

            $this->Post->Comment->create(); 

            if ($this->Post->Comment->save($this->request->data)) {

                $this->Flash->success(__('The Comment has been saved.'.$msgImg));
                $this->redirect(array('action'=>'view',$id));
            }

            $this->Flash->error(__('The Comment could not be saved. Please, try again.'));
        }

        $post = $this->Post->read(null, $id);
        $this->set('post', $post);
    }

    public function add() {

        if ($this->request->is('post')) {

            $size=$this->request->data["Post"]["image"]["size"];
            $type=$this->request->data["Post"]["image"]["type"];
            
            if(parent::checkImgSizeType($type,$size)){ 

                if($this->data && is_uploaded_file($this->request->data['Post']['image']['tmp_name'])){

                    $this->request->data["Post"]["img_name"] = $this->request->data["Post"]["image"]["name"];

                    $fileData = fread(
                    fopen($this->data["Post"]["image"]["tmp_name"], "r"),
                    $this->request->data["Post"]["image"]["size"]
                    );

                    $this->request->data["Post"]["image"] = $fileData;
                    $msgImg = "Image uploaded successfully.";
                }
            }
            else{

                $msgImg = "Image uploaded unsuccessfully.";
                $this->request->data["Post"]["image"] = NULL;
            }

            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->Post->create();

            if ($this->Post->save($this->request->data["Post"])) {

                $this->Flash->success(__('Your post has been saved. '.$msgImg));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('This post not be saved. Please, try again.'));
        }
    }

    public function edit($id = null) {

	    if (!$id) {

	        throw new NotFoundException(__('Invalid post'));
	    }

	    $post = $this->Post->findById($id);

	    if (!$post) {

	        throw new NotFoundException(__('Invalid post'));
	    }

	    if ($this->request->is(array('post', 'put'))) {

	        $this->Post->id = $id;
	        $user['id'] = $this->Auth->user('id');

	        if($this->isAuthorized($user)){
                    
                $size=$this->request->data["Post"]["image"]["size"];
                $type=$this->request->data["Post"]["image"]["type"];
                
                if(parent::checkImgSizeType($type,$size)){ 

                    if($this->data && is_uploaded_file($this->request->data['Post']['image']['tmp_name'])){

                        $this->request->data["Post"]["img_name"] = $this->request->data["Post"]["image"]["name"];

                        $fileData = fread(
                        fopen($this->data["Post"]["image"]["tmp_name"], "r"),
                        $this->request->data["Post"]["image"]["size"]
                        );

                        $this->request->data["Post"]["image"] = $fileData;
                        }
                        $msgImg = "Uploaded image successfully.";
                }
                else{

                    $query = $this->Post->find('list', array('conditions' =>array('Post.id LIKE' => $id),'fields' => array('Post.id', 'Post.image')));
                    $image = $query[$id];
                    $this->request->data["Post"]["image"] = $image;
                    $msgImg = "Uploaded image unsuccessfully.";
                }

                $this->Post->create();

		        if ($this->Post->save($this->request->data["Post"])) {

		            $this->Flash->success(__('Your post has been updated. '.$msgImg));
		            return $this->redirect(array('action' => 'index'));
		        }

	  		}

	        $this->Flash->error(__('Unable to update this post.'));
            return $this->redirect(array('action' => 'index'));
	    }

	    if (!$this->request->data) {

	        $this->request->data = $post;
	    }
	}

    public function delete($id) {

	    if ($this->request->is('get')) {

	        throw new MethodNotAllowedException();
	    }

	    $user['id'] = $this->Auth->user('id');

	    if($this->isAuthorized($user)){

		    if ($this->Post->delete($id)) {

		        $this->Flash->success(__('The post with id: %s has been deleted.', h($id)));
		    }

	    } else {

	        $this->Flash->error(__('The post with id: %s could not be deleted.', h($id)));
	    }

	    return $this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user) {

	    if ($this->action === 'add') {

	        return true;
	    }

	    if (in_array($this->action, array('edit', 'delete'))) {

	    	if ($this->Auth->user('role')=='admin'){

	    		return true;
	    	}

	        $postId = (int) $this->request->params['pass'][0];

	        if ($this->Post->isOwnedBy($postId, $user['id'])) {

	            return true;
	        }
	    }
    }

    public function deleteComment($commentId) {

        if ($this->request->is('get')) {

            throw new MethodNotAllowedException();
        }

        $query1 = $this->Post->Comment->find('list', array('conditions' =>array('Comment.id LIKE' => $commentId),'fields' => array('Comment.id', 'Comment.user_id')));
        $commentOwnerId = $query1[$commentId];
        $query2 = $this->Post->Comment->find('list', array('conditions' =>array('Comment.id LIKE' => $commentId),'fields' => array('Comment.id', 'Comment.foreign_id')));
        $postId = $query2[$commentId];

        if($this->isAuthorizedForComment($commentOwnerId)){

            if ($this->Post->Comment->delete($commentId)) {

                $this->Flash->success(__('The comment with id: %s has been deleted.', $commentId));
            }
        } else {

            $this->Flash->error(__('The comment with id: %s could not be deleted.',$commentId));
        }

        return $this->redirect(array('action'=>'view',$postId));
    }

    public function editComment($commentId) {

        if (!$commentId) {

            throw new NotFoundException(__('Invalid comment'));
        }

        $comment = $this->Post->Comment->findById($commentId);

        if (!$comment) {

            throw new NotFoundException(__('Invalid comment'));
        }

        if ($this->request->is(array('comment', 'put'))) {

            $this->Post->Comment->id = $commentId;
            $query1 = $this->Post->Comment->find('list', array('conditions' =>array('Comment.id LIKE' => $commentId),'fields' => array('Comment.id', 'Comment.user_id')));
            $commentOwnerId = $query1[$commentId];
            $query2 = $this->Post->Comment->find('list', array('conditions' =>array('Comment.id LIKE' => $commentId),'fields' => array('Comment.id', 'Comment.foreign_id')));
            $postId = $query2[$commentId];
                    
            $size=$this->request->data["Comment"]["image"]["size"];
            $type=$this->request->data["Comment"]["image"]["type"];
                
            if(parent::checkImgSizeType($type,$size)){ 

                if($this->data && is_uploaded_file($this->request->data['Comment']['image']['tmp_name'])){

                    $this->request->data["Comment"]["img_name"] = $this->request->data["Comment"]["image"]["name"];

                    $fileData = fread(
                    fopen($this->data["Comment"]["image"]["tmp_name"], "r"),
                    $this->request->data["Comment"]["image"]["size"]
                    );

                    $this->request->data["Comment"]["image"] = $fileData;
                    }
                    $msgImg = "Uploaded image successfully.";
            }
            else{

                $query3 = $this->Post->Comment->find('list', array('conditions' =>array('Comment.id LIKE' => $commentId),'fields' => array('Comment.id', 'Comment.image')));
                $image = $query3[$commentId];
                $this->request->data["Comment"]["image"] = $image;
                $msgImg = "Uploaded image unsuccessfully.";
            }        

            if($this->isAuthorizedForComment($commentOwnerId)){

                if ($this->Post->Comment->save($this->request->data)) {

                    $this->Flash->success(__('Your comment has been updated. '.$msgImg));
                    return $this->redirect(array('action'=>'view',$postId));
                }
            }

            $this->Flash->error(__('Unable to update this comment.'));
            return $this->redirect(array('action'=>'view',$postId));
        }

          if (!$this->request->data) {

            $this->request->data = $comment;
        }
    }

    public function isAuthorizedForComment($ownerId) {

        if (in_array($this->action, array('deleteComment','editComment'))) {

            if ($this->Auth->user('role')=='admin'){
                return true;
            }
            if ($this->Auth->user('id') == $ownerId) {
                return true;
            }
        }
	}
}
