<h1>Edit Comment</h1>
<?php
	echo $this->Form->create('Comment',array('enctype' => 'multipart/form-data'));
	echo $this->Form->input('body', array('rows' => '3'));
	echo $this->Form->input('id', array('type' => 'hidden'));
?>
<?php if($this->data['Comment']['image'] != NULL) :?>
	<img src="data:image/jpeg;base64,<?php echo base64_encode($this->data['Comment']['image']); ?>" align="middle" border="1" height="60" width="60"/>
<?php endif; ?>
<?php
	echo $this->Form->file('image');
	echo $this->Form->end('Save Comment');
?>
