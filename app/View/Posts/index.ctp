<?php $this->fetch('banner'); ?>
<?php

$paginator = $this->Paginator;
$paginator->options(array('url' => $this->passedArgs));
?>

<p>
    <img src="data:image/jpeg;base64,<?php echo base64_encode(AuthComponent::user('image')); ?>" align="middle" border="1" height="60" width="60"/> 
    <?php if (AuthComponent::user('id')) echo AuthComponent::user('username');?>
    |
    <?php echo $this->Html->Link('Logout',array('controller'=> 'Users','action' =>'logout'));?>
    |
    <?php echo $this->Html->link('Add Post', array('action' => 'add'));?>
</p>

<p>
    <h1><b>Blog posts</b></h1>
    <table>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Actions</th>
            <th>Created</th>
        </tr>
        <?php
            if(empty($posts)){ echo "<tr><td colspan='4'>No Search Data.</tr></td>";}
            foreach ($posts as $post): 
        ?>
        <tr><tr>
            <td><?php echo $post['Post']['id']; ?></td>
            <td>
                <?php
                    echo $this->Html->link(
                        $post['Post']['title'],
                        array('action' => 'view', $post['Post']['id'])
                    );
                ?>
            </td>
            <td>
                <?php
                if((AuthComponent::user('id') == $post['Post']['user_id']) || (AuthComponent::user('role')=='admin')){
                    echo $this->Html->link(
                        'Edit', array('action' => 'edit', $post['Post']['id'])
                    );
                    echo "&nbsp;";
                    echo $this->Form->postLink(
                        'Delete',
                        array('action' => 'delete', $post['Post']['id']),
                        array('confirm' => 'Are you sure?')
                    );
                }
                ?>
            </td>
            <td>
                <?php echo $post['Post']['created']; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>

    <!-- Pagination -->
    <?php

    echo "<div class='paging'>";
     
            echo $paginator->first("First");
             
            if($paginator->hasPrev()){

                echo $paginator->prev("Prev");
            }
             
            echo $paginator->numbers(array('modulus' => 2));

            if($paginator->hasNext()){

                echo $paginator->next("Next");
            }

            echo $paginator->last("Last");
         
        echo "</div>";
    ?>
</p>
<hr>

<p>
    <!-- Search Form -->
    <form id="SearchForm" method="post" action="/posts/search">
        <fieldset>
            <h1><b>Post Search</b></h1>
        <?php

            echo $this->Form->input('Search.id');
            echo $this->Form->input('Search.title');
            echo $this->Form->submit('Search');
        ?>
        </fieldset>
    </form>
</p>

