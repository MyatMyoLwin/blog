<h1>Edit Post</h1>
<?php
	echo $this->Form->create('Post',array('enctype' => 'multipart/form-data'));
	echo $this->Form->input('title');
	echo $this->Form->input('body', array('rows' => '3'));
	echo $this->Form->input('id', array('type' => 'hidden'));
?>
&nbsp;&nbsp;
<?php if($this->data['Post']['image'] != NULL) :?>
	<img src="data:image/jpeg;base64,<?php echo base64_encode($this->data['Post']['image']); ?>" align="middle" border="1" height="60" width="60"/>
<?php endif; ?>
<?php 
	echo $this->Form->file('image');
	echo $this->Form->end('Save Post');
?>