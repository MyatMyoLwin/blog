<p>
    <img src="data:image/jpeg;base64,<?php echo base64_encode(AuthComponent::user('image')); ?>" align="middle" border="1" height="60" width="60"/>
    User Name&nbsp;:&nbsp;
    <?php if (AuthComponent::user('id')) echo AuthComponent::user('username');?>
    |
    <?php echo $this->Html->Link('Logout',array('controller'=> 'Users','action' =>'logout'));?>
    |
    <?php echo $this->Html->Link('Home',array('controller'=> 'Posts','action' =>'index'));?>
</p>
<hr>

<p>
<h1><b><?php echo h($post['Post']['title']); ?></b></h1>
<small>Created: <?php echo $post['Post']['created']; ?></small>
</p>
<p><?php echo h($post['Post']['body']); ?></p>
<p>
<?php if($post['Post']['image'] != NULL) :?>
<img src="data:image/jpeg;base64,<?php echo base64_encode($post['Post']['image']); ?>"height="60" width="60"/>
<?php endif; ?>
</p>
<!-- show comments-->
<table>
  	<tr>
        <th>Name</th>
        <th style="max-width:300px">Comments</th>
        <th>Actions</th>
   	</tr>

	<?php 

	sort($post['Comment']);
	foreach ($post['Comment'] as $key => $comment): 

	?>
    <tr>
        <td><?php echo $comment['name']; ?></td>
        <td style="max-width:300px;overflow:auto;">
            <?php echo $comment['body'];?><br>
            <?php if($comment['image'] != NULL) :?>
            <img src="data:image/jpeg;base64,<?php echo base64_encode($comment['image']); ?>" height="42" width="42"/>
            <?php endif; ?>
        </td>
        <td>
            <?php
            if((AuthComponent::user('id') == $comment['user_id']) || (AuthComponent::user('role')=='admin')){
                echo $this->Form->postLink(
                    'Edit',
                    array('action' => 'editComment', $comment['id'])
                );
                echo "&nbsp;";
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'deleteComment', $comment['id']),
                    array('confirm' => 'Are you sure?')
                );
            }
            ?>

        </td>
     </tr>
    <?php endforeach; ?>
</table>
<!-- end show comments-->

<?php 
    echo $this->Form->create(
        'Comment',array(
            'enctype' => 'multipart/form-data',
            'url'=>array('controller'=>'posts','action'=>'view',$post['Post']['id'])
        )
    );
?>
   	<fieldset>
        <legend>
            <?php echo __('Comment Here'); ?>
        </legend>
        <?php 
            echo $this->Form->input('Comment.name',array('default'=>AuthComponent::user('username'),'disabled' => 'disabled'));
            echo $this->Form->input('Comment.body'); 
            echo $this->Form->file('image');
        ?>
	<fieldset>
<?php echo $this->Form->end('Submit');?>