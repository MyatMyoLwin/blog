<h1>Add Post</h1>

<?php

	echo $this->Form->create('Post',array('enctype' => 'multipart/form-data'));
	echo $this->Form->input('title');
	echo $this->Form->input('body', array('rows' => '3'));
	echo $this->Form->file('image');
	echo $this->Form->end('Save Post');
