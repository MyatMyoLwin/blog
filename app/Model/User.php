<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'author')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )/*,
        'image' => array(
            'type'=>array(
            'rule'=>array('extension',array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
            'message'=>'Please enter a valid image!'
            ),
            'fileSize'=>array(
            'rule' => array('fileSize', '<=', '1MB'),
            'message'=>'Image is too big please try again!'
            )
        )*/
    );

    public function beforeSave($options = array()) {

        if (!parent::beforeSave($options)) {

            return false;
        }

        if (isset($this->data[$this->alias]['password'])) {

            $hasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $hasher->hash($this->data[$this->alias]['password']);
        }

        return true;
    }

}
